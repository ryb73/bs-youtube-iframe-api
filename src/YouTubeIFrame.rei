type t;
type player;
type event('a) = {
     data: 'a,
     target: player,
 };
type playerState = Unstarted | Ended | Playing | Paused | Buffering | VideoCued;

let injectScript: unit => ReDom.Types.element(ReDom.Types.script);
let ready: Js.Promise.t(t);
let makePlayer:
    (~height: string=?, ~width: string=?, ~videoId: string=?, ~autoplay: bool=?,
     ~controls: bool=?, ~disablekb: bool=?, ~end_: int=?, ~fullscreen: bool=?,
     ~origin: string=?, ~start: int=?, ~onReady: event(unit) => unit=?,
     ~onStateChange: (event(playerState) => unit)=?,
     string, t) => player;

let loadVideoById:
    (~endSeconds: int=?, ~startSeconds: int=?,
     ~suggestedQuality:
         [< `default | `hd1080 | `hd720 | `highres | `large | `medium | `small ]=?,
     string, player) => unit

let destroy: player => unit;
let playVideo: player => unit;
let pauseVideo: player => unit;
let stopVideo: player => unit;

/** (seconds, allowSeekAhead, player) => unit */
let seekTo: float => bool => player => unit;

let setSize: (~width: float) => (~height: float) => player => unit;
let getCurrentTime: player => float;
