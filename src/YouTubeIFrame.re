open ReDom;
open Belt.Option;

type player;

type playerState = Unstarted | Ended | Playing | Paused | Buffering | VideoCued;

[@bs.deriving abstract]
type playerVars = {
    [@bs.optional] autoplay: int,
    [@bs.optional] controls: int,
    [@bs.optional] disablekb: int,
    [@bs.optional] [@bs.as "end"] end_: int,
    [@bs.optional] fs: int,
    [@bs.optional] origin: string,
    [@bs.optional] start: int,
};
let mkPlayerVars = playerVars;

[@decco.decode]
type event('a) = {
    data: 'a,
    target: [@decco.codec Decco.Codecs.magic] player,
};

[@bs.deriving abstract]
type events = {
    [@bs.optional] onReady: Js.Json.t => unit,
    [@bs.optional] onStateChange: Js.Json.t => unit,
};
let mkEvents = events;

[@bs.deriving abstract]
type playerOptions = {
    [@bs.optional] height: string,
    [@bs.optional] width: string,
    [@bs.optional] videoId: string,
    playerVars: playerVars,
    events: events
};

let injectScript = () =>
    Script.create()
    |> Script.setSrc("https://www.youtube.com/iframe_api")
    |> Element.appendChild(~parent=Document.body(), ~child=_)

type t = unit;
let ready = Js.Promise.make((~resolve, ~reject as _) => {
    Window.set("onYouTubeIframeAPIReady", () => {
        let u = ();
        resolve(.u);
    });
});

let encodeBool = fun
    | true => 1
    | false => 0;

let decodeState = fun
    | -1 => Unstarted
    | 0 => Ended
    | 1 => Playing
    | 2 => Paused
    | 3 => Buffering
    | 5 => VideoCued
    | i => Js.Exn.raiseError("Unrecognized player state: " ++ string_of_int(i));

let wrapOnReady = (cb) =>
    (event) =>
        event
        |> event_decode(Decco.unitFromJson)
        |> Belt.Result.getExn
        |> cb;

let wrapOnStateChange = (cb) =>
    (event) =>
        event
        |> event_decode(Decco.intFromJson)
        |> Belt.Result.getExn
        |> (event => { ...event, data: decodeState(event.data) })
        |> cb;

[@bs.new] [@bs.scope "YT"] external newPlayer: string => playerOptions => player = "Player";
let makePlayer = (~height=?, ~width=?, ~videoId=?, ~autoplay=?, ~controls=?,
                  ~disablekb=?, ~end_=?, ~fullscreen=?, ~origin=?, ~start=?,
                  ~onReady=?, ~onStateChange=?, containerId, ())
=>
    newPlayer(containerId, playerOptions(
        ~height?, ~width?, ~videoId?,
        ~playerVars=mkPlayerVars(
            ~autoplay=?autoplay -> map(encodeBool),
            ~controls=?controls -> map(encodeBool),
            ~disablekb=?disablekb -> map(encodeBool),
            ~fs=?fullscreen -> map(encodeBool),
            ~end_?, ~origin?, ~start?,
            ()
        ),
        ~events=mkEvents(
            ~onReady=?onReady -> map(wrapOnReady),
            ~onStateChange=?onStateChange -> map(wrapOnStateChange),
            ()
        ),
        ()
    ));

[@bs.deriving abstract]
type videoOptions = {
    [@bs.optional] endSeconds: int,
    [@bs.optional] startSeconds: int,
    [@bs.optional] suggestedQuality: string,
    videoId: string,
};

let encodeQuality = fun
    | `small => "small"
    | `medium => "medium"
    | `large => "large"
    | `hd720 => "hd720"
    | `hd1080 => "hd1080"
    | `highres => "highres"
    | `default => "default";

[@bs.send.pipe: player] external loadVideoById: videoOptions => unit = "";
let loadVideoById =
    (~endSeconds=?, ~startSeconds=?, ~suggestedQuality=?, videoId, player) =>
        player |>
        loadVideoById(videoOptions(
            ~suggestedQuality=?map(suggestedQuality, encodeQuality),
            ~endSeconds?, ~startSeconds?, ~videoId, ()
        ));

[@bs.send.pipe: player] external destroy: unit = "";

[@bs.send.pipe: player] external playVideo: unit = "";
[@bs.send.pipe: player] external pauseVideo: unit = "";
[@bs.send.pipe: player] external stopVideo: unit = "";
[@bs.send.pipe: player] external seekTo: float => bool => unit = "";

[@bs.send.pipe: player] external setSize: (~width: float) => (~height: float) => unit = "";

[@bs.send.pipe: player] external getCurrentTime: float = "";
