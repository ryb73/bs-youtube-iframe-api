open PromEx;

ReDom.Window.onLoad(_ => YouTubeIFrame.injectScript() |> ignore);

YouTubeIFrame.ready
|> map(yt => ReactDOMRe.renderToElementWithId(<Controls yt />, "container"));
