open React;

let playClicked = (player, _) => YouTubeIFrame.playVideo(player);
let pauseClicked = (player, _) => YouTubeIFrame.pauseVideo(player);
let stopClicked = (player, _) => YouTubeIFrame.stopVideo(player);
let bigClicked = (player, _) => YouTubeIFrame.setSize(~height=780., ~width=1280., player);

let onStateChange = ({ YouTubeIFrame.data }) =>
    switch data {
        | YouTubeIFrame.Unstarted => Js.log("Unstarted")
        | Ended => Js.log("Ended")
        | Playing => Js.log("Playing")
        | Paused => Js.log("Paused")
        | Buffering => Js.log("Buffering")
        | VideoCued => Js.log("VideoCued")
    };

[@react.component]
let make = (~yt, _) => {
    let (player, setPlayer) = useState(() => None);

    let onReady = ({ YouTubeIFrame.target }) =>
        YouTubeIFrame.playVideo(target);

    let makePlayer = _ => {
        let player = YouTubeIFrame.makePlayer(
            ~videoId="uxcMnL3c1os", ~controls=false,
            ~onReady, ~onStateChange, "player", yt
        );
        setPlayer(_ => Some(player));
    };

    let destroyClicked = _ =>
        setPlayer(player => {
            Belt.Option.map(player, player => YouTubeIFrame.destroy(player))
            |> ignore;
            None;
        });

    Belt.Option.map(player, player =>
        <div>
            <button onClick=destroyClicked>(string("Destroy Player"))</button>
            <button onClick=playClicked(player)>(string("Play"))</button>
            <button onClick=pauseClicked(player)>(string("Pause"))</button>
            <button onClick=stopClicked(player)>(string("Stop"))</button>
            <button onClick=bigClicked(player)>(string("Big"))</button>
        </div>
    )
    -> Belt.Option.getWithDefault(<button onClick=makePlayer>(string("Make Player"))</button>)
};
