const path = require("path");

function rel(relPath) {
    return path.resolve(__dirname, relPath)
}

module.exports = {
    mode: "development",

    entry: {
        index: rel("lib/js/test/index"),
    },

    output: {
        path: rel("test"),
        filename: "[name].js",
    },
};
